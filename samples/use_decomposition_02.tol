
#Require DecoTools;

//////////////////////////////////////////////////////////////////////////////
// [1] Definici�n
// 
// Sea un modelo multiplicativa que linealizamos mediante un logaritmo
// de la forma:
//   Log(Observations) = Output = Sum(ExpTerms) + Noise

// Admitamos que son tres t�rminos explicativos y generamos los siguientes
// datos ficticios a modo de ejemplo:

Real PutRandomSeed(2143);
Date begin = y2009;
Date end = y2011m12;

Serie noise = SubSer(Serie Gaussian(3, 1, Monthly), begin, end);
Serie expTerm1 = SubSer(Serie Gaussian(0, 0.5, Monthly), begin, end);
Serie expTerm2 = SubSer(Serie Rand(-0.25, 0.25, Monthly), begin, end);
Serie expTerm3 = SubSer(Serie Gaussian(0.3, 0.1, Monthly), begin, end);

Serie output = noise + expTerm1 + expTerm2 + expTerm3;

// Con estos datos disponemos de la descomposici�n de partida u original:
Set deco1 = [[ output, noise, expTerm1, expTerm2, expTerm3 ]];

// Comprobamos que verifica ser una descomposici�n aditiva v�lida:
Real ok1 = DecoTools::Test(deco1);

//////////////////////////////////////////////////////////////////////////////
// [2] Reagrupar una descomposici�n

// Es bastante com�n querer reagrupar los t�rminos de una descomposici�n
// sumando varios t�rminos explicativos que tienen algo en com�n e incluso
// rest�ndoles un valor de referencia.

// Para ello usaremos la funci�n: 
//  * DecoTools::Deco.BaseDefinition
// y las estructuras: 
//  * DecoTools::@Contribution.Def 
//  * DecoTools::@Contribution.Ref

// Al usar esta funci�n la primera contribuci�n resultante (denominada base)
// asumir� la diferencia que pudiera haber entre el total y la suma del resto
// de contribuciones.

// Pongamos algunos ejemplos:

// (a) Se desea unir  el primer t�rmino explicativo con el noise
//     y denominarlo base

Set deco2a = DecoTools::Deco.BaseDefinition(deco1, [[
  DecoTools::@Contribution.Def("base", Copy(Empty)),
  DecoTools::@Contribution.Def("expTerm2", [[ 
    DecoTools::@Contribution.Ref("expTerm2", 0)
  ]]),
  DecoTools::@Contribution.Def("expTerm3", [[ 
    DecoTools::@Contribution.Ref("expTerm3", 0)
  ]])
]]);

// Esto se interpreta como: el t�rmino expTerm2 queda tal cual, lo mismo para
// el expTerm3 y el resto (noise y expTerm1) se suman creando la base.

// (b) Se desea dejar el noise como t�rmino base y sumar los t�rminos 1 y 3

Set deco2b = DecoTools::Deco.BaseDefinition(deco1, [[
  DecoTools::@Contribution.Def("noise", Copy(Empty)),
  DecoTools::@Contribution.Def("expTerm13", [[ 
    DecoTools::@Contribution.Ref("expTerm1", 0),
    DecoTools::@Contribution.Ref("expTerm3", 0)
  ]]),
  DecoTools::@Contribution.Def("expTerm2", [[ 
    DecoTools::@Contribution.Ref("expTerm2", 0)
  ]])
]]);

// (c) Se desea sumar los t�rminos 1 y 2 y restar al t�rmino 3 su media,
//     dej�ndola junto con le noise como contribuci�n base.

Set deco2c = DecoTools::Deco.BaseDefinition(deco1, [[
  DecoTools::@Contribution.Def("noise", Copy(Empty)),
  DecoTools::@Contribution.Def("expTerm12", [[ 
    DecoTools::@Contribution.Ref(2, 0), // 2 -> expTerm1
    DecoTools::@Contribution.Ref(3, 0)  // 3 -> expTerm2
  ]]),
  DecoTools::@Contribution.Def("expTerm3_mean0", [[ 
    DecoTools::@Contribution.Ref(4, Code AvrS) // 4 -> expTerm3
  ]])
]]);

// En este ejemplo nos referimos a las contribuciones de referencia con sus
// �ndices y no con sus nombres.
// N�tese que la contribuci�n 1 es el segundo elemento (el noise). 

//////////////////////////////////////////////////////////////////////////////
// [3] Descomposici�n can�nica

// La descomposici�n can�nica habitual es la que podr�amos denominar
// descomposici�n can�nica con contribuci�n base, en la que la primera
// contribuci�n se mantiene fija (habitualmente el noise) y se permutan
// el resto de contribuciones.

// La descomposici�n base-can�nica de deco1, considerando que �sta se obtuvo
// linealizando con el logaritmo es:

Set deco3 = DecoTools::Deco.BaseCanonical(deco1, FindCode("Real", "Exp"));

// Esta descomposici�n se hace recorriendo todas las permutaciones de las
// contribuciones. Cuando el n�mero de contribuciones es mayor de:
Real DecoTools::PermutationsLimit;
// y el n�mero de permutaciones ser�:
Real Factorial(DecoTools::PermutationsLimit+1);
// o mayor se usar� s�lo una muestra de las permutaciones de tama�o:
Real DecoTools::SampleSize;

// Afortunadamente para la transformaci�n exponenecial (modelos 
// multiplicativos) disponemos de un algoritmo eficiente con el que obtener
// la descomposici�n exacta, sin tener que muestrear:

Set deco3_2 = DecoTools::Deco.Exponential.BaseCanonical(deco1); // == deco3

//////////////////////////////////////////////////////////////////////////////
// [4] DueTo

// B�sicamente un dueto es una descomposici�n propia de series temporales
// donde cada instante del total se explica repecto a un instante anterior
// como la suma de �ste y las diferencias debidas a las contribuciones.

// (a) Por ejemplo, el dueto respecto al mes anterior de deco1 ser�a:

Set deco4a = DecoTools::DueTo.Period(deco1, 1);

// (b) Tambi�n podemos hacer el dueto de la descomposici�n de las 
//     observaciones (Exp(output)) a partir de deco3

Set deco4b = DecoTools::DueTo.Period(deco3, 1);

// Hay un mecanismo de obtener una descomposici�n de tipo due-to para las
// observaciones (en t�rminos originales, no lienalizados) sin pasar por una
// descomposici�n previa (como la descomposici�n can�nica).
// A este mecanismo podr�amos denominarlo dueto generalizado y construyen el 
// dueto de manera proporcional al dueto que obtendr�amos sin aplicar la 
// transformaci�n.

// (c) Obtengamos el dueto generalizado de deco1. 
// El resultado es comparable, pero distinto al anterior deco4b.

Set deco4c = DecoTools::GeneralizedDueTo.Period(deco1,
  FindCode("Serie", "Exp"), 1);

// A menudo el dueto de inter�s es en un fechado distinto, en el que las
// series presenta periodicidad.
// En el ejemplo que nos ocupa podr�a ser el fechado anual (Yearly).

// Para calcular este dueto peri�dico es necesario hacer el c�lculo
// en fechado mensual con un periodo 12 y luego agregar los resultados
// haciendo un cambio de fechado.

// N�tese que en el caso de los dueto generalizados NO es lo mismo que 
// agregar primero y hacer el dueto despu�s.

// (d) Al dueto generalizado y peri�dico se le conoce en general como dueto
//     o dueto secuencial sin m�s calificativos.
//     Calculemos este 'dueto' para deco1

Set deco4d = {
  Set aux = DecoTools::GeneralizedDueTo.Period(deco1,
    FindCode("Serie", "Exp"), 12);
  Set DecoTools::Deco.Serie.DatCh(aux, Yearly, SumS)
};

// (e) Un dueto periodico como el anterior (pero distinto) puede 
//     obtenerse tambi�n desde la descomposici�n can�nica deco3

Set deco4e = {
  Set aux = DecoTools::DueTo.Period(deco3, 12);
  Set DecoTools::Deco.Serie.DatCh(aux, Yearly, SumS)
};

// N�tese que en este caso (sin usar dueto generalizado) los dos pasos
// se pueden conmutar ya que son operaciones lineales.

Set deco4e_2 = {
  Set aux = DecoTools::Deco.Serie.DatCh(deco3, Yearly, SumS);
  DecoTools::DueTo.Period(aux, 1)
}; // == deco4e

//////////////////////////////////////////////////////////////////////////////
// [5] Representaciones relativas

// En ocasiones, sobre todo en el caso de los dueto, es interesante
// disponer de las descomposiciones en unidades relativas.
// A continuaci�n presentamos dos posibilidades.

// (a) Representar la diferencia entre el total y la base (primera 
//     contribuci�n) de una descomposici�n (normalmente un dueto) en 
//     unidades relativas a la base.
//     As� de:
//       {total, base, contrib1, contrib2, ....}
//     obtenemos:
//       {total/base-1, contrib1/base, contrib2/base, ...}

Set deco5a = DecoTools::Deco.BaseRelative(deco4d);

// Esta representaci�n (pero ordenada) se utiliza para los gr�ficos de dueto
// o gr�ficos en cascada.

// (b) Representar una descomposici�n en unidades relativas al total.
//     As� de:
//       {total, contrib1, contrib2, ....}
//     obtenemos:
//       {1, contrib1/total, contrib2/total, ...}

Set deco5b = DecoTools::Deco.TotalRelative(deco4d);

//////////////////////////////////////////////////////////////////////////////
// [6] Uso en MMS

// MMS incorpora algunos mecanismos de descomposici�n implementados
// sobre la descomposici�n de partida:
//   mmsDeco0 = {output, noise, expTerm1, expTerm2, ...}

// Tomamos deco1 como la decomposici�n de partida de MMS y
// la siguiente reagrupaci�n de las contribuciones:

Set contributions = [[
  DecoTools::@Contribution.Def("noise", Copy(Empty)),
  DecoTools::@Contribution.Def("expTerm13", [[ 
    DecoTools::@Contribution.Ref("expTerm1", 0),
    DecoTools::@Contribution.Ref("expTerm3", 0)
  ]]),
  DecoTools::@Contribution.Def("expTerm2", [[ 
    DecoTools::@Contribution.Ref("expTerm2", 0)
  ]])
]];

// (a) GetOutput.Decomposition(contributions)
//     Simplemente usa: Deco.BaseDefinition 

Set deco6a = DecoTools::Deco.BaseDefinition(deco1, contributions);

// (b) GetObservations.Decomposition(contributions)
//     Combina: Deco.BaseDefinition y Deco.BaseCanonical

Set deco6b = {
  Set aux = DecoTools::Deco.BaseDefinition(deco1, contributions);
  DecoTools::Deco.BaseCanonical(deco1, FindCode("Real", "Exp"))
};

// La transformaci�n la obtiene MMS del submodelo correspondiente

// (c) GetObservations.SequentialDueTo(contributions)
//     Combina: Deco.BaseDefinition,  DecoTools::GeneralizedDueTo.Period,
//              DecoTools::Deco.Serie.DatCh y DecoTools::Deco.BaseRelative

Set deco6C = {
  Set aux = DecoTools::Deco.BaseDefinition(deco1, contributions);
  Set aux2 = DecoTools::GeneralizedDueTo.Period(deco1,
    FindCode("Serie", "Exp"), 12);
  Set aux3 = DecoTools::Deco.Serie.DatCh(aux2, Yearly, SumS);
  DecoTools::Deco.BaseRelative(aux3)
};

// El periodo 12 y el fechado arm�nico los obtiene MMS a partir de la
// estructura ARIMA del submodelo correspondiente

//////////////////////////////////////////////////////////////////////////////
