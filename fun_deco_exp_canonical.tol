
//////////////////////////////////////////////////////////////////////////////
// Deco.Exponential.BaseCanonical
// Deco.Exponential.Canonical

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.Exponential.BaseCanonical%es Devuelve la descomposición aditiva \
//BBL canónica con contribución base obtenida al aplicar la transformación \
//BBL exponencial. 
Set Deco.Exponential.BaseCanonical(Set decomposition)
//////////////////////////////////////////////////////////////////////////////
{
  Set decomposition_B = _GetGrammar.Method(decomposition,
    _Deco.Exponential.BaseCanonical_Real, Copy(Empty));
  Set names = _Deco.Exponential.BaseCanonical.GetNames(decomposition);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.Exponential.Canonical%es Devuelve la descomposición aditiva \
//BBL canónica obtenida al aplicar la transformación exponencial. 
Set Deco.Exponential.Canonical(Set decomposition)
//////////////////////////////////////////////////////////////////////////////
{
  Set decomposition_B = _GetGrammar.Method(decomposition,
    _Deco.Exponential.Canonical_Real, Copy(Empty));
  Set names = _Deco.Exponential.Canonical.GetNames(decomposition);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Exponential.BaseCanonical.GetNames(Set decomposition)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetBaseNames(decomposition);
  If(Deco.Rename.Active, {
    Text decName = names[1]<<"_Exp.BaseCanonical";
    Set addNames = [[
      names[2][1]<<"_Exp";
      names[2][2]<<"_Exp"
    ]] << For(3, Card(names[2]), Text (Real i) {
      names[2][i]<<"_BaseCanonicalEffect"
    });
    [[ decName, addNames ]]
  }, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Exponential.Canonical.GetNames(Set decomposition)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetNames(decomposition);
  If(Deco.Rename.Active, {
    Text decName = names[1]<<"_Exp.Canonical";
    Set addNames = [[
      names[2][1]<<"_Exp";
      names[2][1]<<"_InitialEffect"
    ]] << For(2, Card(names[2]), Text (Real i) {
      names[2][i]<<"_CanonicalEffect"
    });
    [[ decName, addNames ]]
  }, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Exponential.BaseCanonical_Real(Set decomposition)
//////////////////////////////////////////////////////////////////////////////
{
  Real _Test_Real(decomposition);
  Real length = Card(decomposition);
  Real total = decomposition[1];
  Real base = decomposition[2];
  Set contributions = ExtractByIndex(decomposition, Range(3, length, 1));
  Real ef.total = Exp(total);
  Real ef.base = Exp(base);
  Set effects = EvalSet(
    _Deco.Exponential.RelativeEffects(contributions),
    Real (Real relEffect) { Real effect = ef.base * relEffect });
  //! Comprobación de la sinergía...
  Real synergy = ef.total-(ef.base+SetSum(effects));
  [[ ef.total, ef.base ]] << effects
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Exponential.Canonical_Real(Set decomposition)
//////////////////////////////////////////////////////////////////////////////
{
  Real _Test_Real(decomposition);
  Real length = Card(decomposition);
  Real total = decomposition[1];
  Real base = 0;
  Set contributions = ExtractByIndex(decomposition, Range(2, length, 1));
  Real ef.total = Exp(total);
  Real ef.base = Exp(base);
  Set effects = EvalSet(
    _Deco.Exponential.RelativeEffects(contributions),
    Real (Real relEffect) { Real effect = ef.base * relEffect });
  //! Comprobación de la sinergía...
  Real synergy = ef.total-(ef.base+SetSum(effects));
  [[ ef.total, ef.base ]] << effects
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.Exponential.RelativeEffects(Set contributions)
//////////////////////////////////////////////////////////////////////////////
{
  Set ef.contribs = EvalSet(contributions, Real (Real c) { Exp(c) });
  Set ef.contribsR = Select(ef.contribs, Real (Real c) { 
    And(Not(IsUnknown(c)),c!=1) });
  Real c.length = Card(ef.contribs);
  Real c.lengthR = Card(ef.contribsR);
  Real kR=0;
  Set effects = For(1, c.length, Real (Real k) {
    Real effect = Case(
      IsUnknown(ef.contribs[k]), ?,
      ef.contribs[k]==1, 0,
      True, {
        Real kR := kR+1;
        Set indices = Range(1, kR-1, 1) << Range(kR+1, c.lengthR, 1);
        Real If(Card(indices)==0, {
          Real ef.contribs[k]-1
        }, {
          Set data = ExtractByIndex(ef.contribsR, indices);
          Real foreignEffect = _Deco.Exponential.ForeignEffect(data);
          Real foreignEffect * (ef.contribs[k]-1)
        })
      }
    )
  })
};

//////////////////////////////////////////////////////////////////////////////
Real _Deco.Exponential.ForeignEffect(Set data)
//////////////////////////////////////////////////////////////////////////////
{
  Real M = Card(data);
  Set sK = For(1, M, Real (Real k){ SetMoment(data,k) });
  Set sQ = [[ Real 1 ]];
  Set For(1, M, Real (Real p){
    Real Qp = SetSum(For(1, p, Real (Real i){
      ((-1)^(i+1))*sK[i]*sQ[p-i+1]
    }))*M/p;
    Set (sQ:=sQ<<[[Copy(Qp)]]);
  0});
  Real SetAvr(For(0, M, Real (Real p){ 
    sQ[p+1]/Comb(M,p)
  }))
};

//////////////////////////////////////////////////////////////////////////////
