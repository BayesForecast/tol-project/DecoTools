
//////////////////////////////////////////////////////////////////////////////
// Deco.FirstIn

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.FirstIn%es Devuelve la descomposición aditiva first-in obtenida \
//BBL al aplicar la transformación.
Set Deco.FirstIn(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set decomposition_B = _GetGrammar.Method(decomposition,
    _Deco.FirstIn_Real, [[transf]]);
  Set names = _Deco.FirstIn.GetNames(decomposition, transf);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.FirstIn.GetNames(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetNames(decomposition);
  Real hasEffect0 = (_ZeroEffect(decomposition, transf)!=0);
  If(Deco.Rename.Active, {
    Text transfName = _GetNameIf(Name(transf), "Transf");
    Text decName = names[1]<<"_FirstIn";
    Set addNames = [[
      names[2][1]<<"_"<<transfName
    ]] << For(2-hasEffect0, Card(names[2]), Text (Real i) {
      names[2][i]<<"_FirstIn"
    }) << [[ 
      names[1]<<"_Synergy"
    ]];
    [[ decName, addNames ]]
  }, {
    [[ names[1], Set [[names[2][1] ]]<<If(hasEffect0, [["Effect0"]], Empty)
      <<Remove(Copy(names[2]),1)<<[["Synergy"]] ]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.FirstIn_Real(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Real Test(decomposition);
  Real length = Card(decomposition);
  Anything total = decomposition[1];
  Anything base = total * 0;
  Set contributions = ExtractByIndex(decomposition, Range(2, length, 1));
  Real ef.total = transf(total);
  Real ef.base = transf(base);
  Set effects = EvalSet(contributions, Real (Anything contribution) {
    Real effect = transf(base + contribution) - ef.base
  });
  Real synergy = ef.total-(ef.base+SetSum(effects));
  [[ ef.total ]] << If(ef.base, [[ ef.base ]], Copy(Empty)) << effects 
    << [[ synergy ]]
};

//////////////////////////////////////////////////////////////////////////////
// Deco.BaseFirstIn

//////////////////////////////////////////////////////////////////////////////
//BBL $Deco.BaseFirstIn%es Devuelve la descomposición aditiva first-in con \
//BBL contribución base obtenida al aplicar la transformación.
Set Deco.BaseFirstIn(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set decomposition_B = _GetGrammar.Method(decomposition,
    _Deco.BaseFirstIn_Real, [[transf]]);
  Set names = _Deco.BaseFirstIn.GetNames(decomposition, transf);
  _Rename(decomposition_B, names)
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.BaseFirstIn.GetNames(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Set names = _GetBaseNames(decomposition);
  If(Deco.Rename.Active, {
    Text transfName = _GetNameIf(Name(transf), "Transf");
    Text decName = names[1]<<"_BaseFirstIn";
    Set addNames = [[
      names[2][1]<<"_"<<transfName;
      names[2][2]<<"_"<<transfName
    ]] << For(3, Card(names[2]), Text (Real i) {
      names[2][i]<<"_BaseFirstIn"
    }) << [[ 
      names[1]<<"_Synergy"
    ]];
    [[ decName, addNames ]]
  }, {
    [[ names[1], names[2]<<[["Synergy"]] ]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Set _Deco.BaseFirstIn_Real(Set decomposition, Code transf)
//////////////////////////////////////////////////////////////////////////////
{
  Real Test(decomposition);
  Real length = Card(decomposition);
  Anything total = decomposition[1];
  Anything base = decomposition[2];
  Set contributions = ExtractByIndex(decomposition, Range(3, length, 1));
  Real ef.total = transf(total);
  Real ef.base = transf(base);
  Set effects = EvalSet(contributions, Real (Anything contribution) {
    Real effect = transf(base + contribution) - ef.base
  });
  Real synergy = ef.total-(ef.base+SetSum(effects));
  [[ ef.total, ef.base ]] << effects << [[ synergy ]]
};

//////////////////////////////////////////////////////////////////////////////
